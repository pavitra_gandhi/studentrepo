package com.example.studentrepo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity  // This tells Hibernate to make a table out of this class
@Table(name = "users")
public class Student {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long rollNo;
	private String className;
	private String name;
	
	
	
	public Student() {
		super();
	}

	public Student(Long rollNo, String className, String name) {
		super();
		this.rollNo = rollNo;
		this.className = className;
		this.name = name;
	}

	public Student(String className, String name) {
		this.className = className;
		this.name = name;
	}

	public Long getRollno() {
		return rollNo;
	}

	public void setRollno(Long rollno) {
		this.rollNo = rollno;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [rollno=" + rollNo + ", className=" + className + ", name=" + name + "]";
	}
	
	
	
}
