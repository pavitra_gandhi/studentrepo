package com.example.studentrepo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.studentrepo.model.Student;
import com.example.studentrepo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
	
	@Autowired
	StudentRepository studrepo;
	
	public List getAllStudents()
	{
		List stud = new ArrayList<>();
		studrepo.findAll().forEach(stud::add);
		return stud;
	}
	
	public Student getStudent(long rollno)
	{
		return studrepo.findById(rollno).orElse(null);
	}
	
	public void addStudent(Student student)
	{
		studrepo.save(student);
	}
	
	public void updateStudent(long rollno, Student student)
	{
		studrepo.save(student);
	}
	
	public void deleteStudent(long rollno)
	{
		studrepo.deleteById(rollno);
	}
	
	public List<Student> searchStudent(String name)
	{
		List<Student> stud = new ArrayList<>();
		studrepo.findAll().forEach(stud::add);
		return stud.stream().filter(t -> t.getName().contains(name)).collect(Collectors.toList());
	}
	
}
