package com.example.studentrepo.batch;

import com.example.studentrepo.model.Student;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<Student, Student> {
    private static final Map<String, String> CLASS_NAME =
            new HashMap<>();

    public Processor()
    {
        CLASS_NAME.put("1st", "First");
        CLASS_NAME.put("2nd", "Second");
        CLASS_NAME.put("3rd", "Third");
        CLASS_NAME.put("4th", "Fourth");
    }

    @Override
    public Student process(Student student) throws Exception
    {
        String classCode = student.getClassName();
        String className = CLASS_NAME.get(classCode);
        student.setClassName(className);
        System.out.println(String.format("Converted from {%s} to {%s}", classCode, className));
        return student;
    }
}
