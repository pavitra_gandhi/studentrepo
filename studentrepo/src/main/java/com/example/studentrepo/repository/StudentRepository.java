package com.example.studentrepo.repository;

import com.example.studentrepo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{

}
//public interface StudentRepository{
// 
//}