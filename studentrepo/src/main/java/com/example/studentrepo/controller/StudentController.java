package com.example.studentrepo.controller;

import java.util.List;

import com.example.studentrepo.model.Student;
import com.example.studentrepo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
	
	
	@GetMapping("/hello")
	@ResponseBody
	public String Hello()
	{
		return "Hello World";
		
	}
	
//	@GetMapping("/") // to get the values again and again when values are fetched from fetch.jsp
//	public String getStudent()
//	{
////		return "fetch";
//		
//	}
	
	
//	@Autowired
//	StudentRepository studrepo;
//	
	
//	@GetMapping("/students")
//	public List getAllStudents()
//	{
//		List stud = new ArrayList<>();
//		studrepo.findAll().forEach(stud::add);
//		return stud;		
//	}
//	
//	@RequestMapping("/details")
//	public String getStudent(Student student)
//	{
//		studrepo.save(student);
//		return "fetch";
//		
//	}
//	
//	@GetMapping("/getstudent")
//	public String viewDetails()
//	{
//		return "show";
//	}
//	
//	@PostMapping("/getdetails")
//	public ModelAndView viewDetails(@RequestParam("rollno") long sroll)
//	{
//		ModelAndView mv = new ModelAndView("retrieve");
//		Student student = studrepo.findById(sroll).orElse(null);
////		if(student != null)
////		{
////			list.add(student);
////		}
//		mv.addObject(student);
//		return mv;
//	}
	private final StudentService studentservice;
	
	@Autowired
	public StudentController(StudentService studentservice) {
		this.studentservice = studentservice;
	}

	@GetMapping("/students")
	public List getAllStudents()
	{
		return studentservice.getAllStudents();
	}
	
	@GetMapping("/students/{name}")
	public List<Student> getAllStudents(@PathVariable String name)
	{
		return studentservice.searchStudent(name);
	}
	
	@RequestMapping("/student/{rollno}")
	public Student getStudent(@PathVariable long rollno)
	{
		return studentservice.getStudent(rollno);
	}
	
	@PostMapping("/students")
	public String addStudents(@RequestBody Student student)
	{
		studentservice.addStudent(student);
		return "Added";
	}
	
	@PutMapping("/student/{rollno}")
	public void updateStudent(@PathVariable long rollno, @RequestBody Student student)
	{
		studentservice.updateStudent(rollno, student);
	}
	
	@DeleteMapping("student/{rollno}")
	public void deleteStudent(@PathVariable long rollno)
	{
		studentservice.deleteStudent(rollno);
	}
//	@GetMapping
//	public List<Student> getStudent()
//	{
//		return studentservice.getStudent();
//	}
}
