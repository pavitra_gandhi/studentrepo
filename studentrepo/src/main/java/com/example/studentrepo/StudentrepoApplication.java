package com.example.studentrepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class StudentrepoApplication{

	public static void main(String[] args) {
		SpringApplication.run(StudentrepoApplication.class, args);
	}
}
